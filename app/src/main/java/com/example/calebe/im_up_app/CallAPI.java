package com.example.calebe.im_up_app;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;

import org.json.JSONObject;

import java.io.File;

public class CallAPI extends AsyncTask<String, Void, Void> {
    String response;
    private Context context;

    String UPLOAD_SERVER = "http://www.kairos-dev.tk/images/";

    public CallAPI(Context context)
    {
        this.context = context;
        AndroidNetworking.initialize(context);
    }

    @Override
    protected void onPreExecute()
    {

    }

    @Override
    protected Void doInBackground(String... params) {
        try {
//            Toast.makeText(context, "UPLOAD:"+params[0].getName(), Toast.LENGTH_LONG).show();

            uploadImage(params[0]);
        } catch (Exception e) {
            response = "Image was not uploaded!";
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        try
        {
//            Log.d("Server Response", response);
//            if(response.contains("success"))
//            {
//
//            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void uploadImage(String file)
    {

        File image = new File(file.toString());
        Log.d("File Upload", image.getName());

        AndroidNetworking.upload("http://www.kairos-dev.tk/images/"+image.getName() )
//        AndroidNetworking.upload(UPLOAD_SERVER)
                .addMultipartFile("image", image)
//                .addMultipartParameter("key","value")
//                .setTag("UploadImage")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                        Log.d("UPLOAD", ""+bytesUploaded+" de "+totalBytes);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Log.d("ServerPostResponse", response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d("ServerPostError", error.toString());
                    }
                });
    }
}

