package com.example.calebe.im_up_app;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class InitialScreen extends AppCompatActivity
{
    static final int REQUEST_IMAGE_CAPTURE = 1;
    String mCurrentPhotoPath = null;

    private ListView listViewMyItems = null;
    private File photoFile = null;
    private  Uri photoURI = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.initial_screen);
        AndroidNetworking.initialize(getApplicationContext());

        populateList();
    }

    private void populateList()
    {

        List<HashMap<String, Object>> aList = new ArrayList<HashMap<String, Object>>();

        String path = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString();
//        Log.d("Files", "Path: " + path);
        File directory = new File(path);
        File[] files = directory.listFiles();

        for (int i = 0; i < files.length; i++)
        {
            HashMap<String, Object> hm = new HashMap<String, Object>();
            hm.put("image_name", files[i].getName());
            hm.put("image_date", "UMA data");
            aList.add(hm);
//            doUpload(files[i]);
            (new CallAPI(this)).execute(files[i].getAbsolutePath());
        }

        String[] from = {"image_name", "image_date"};
        int[] to = {R.id.image_name, R.id.image_date};

        SimpleAdapter simpleAdapter = new SimpleAdapter(getBaseContext(), aList,R.layout.local_items, from, to);
        listViewMyItems = (ListView) findViewById(R.id.listViewMyItems);
        listViewMyItems.setAdapter(simpleAdapter);

        // TESTE DE UPLOAD
//        getServerImages();
//        (new CallAPI(this)).execute(files[0]);
    }

    private void getServerImages()
    {
        AndroidNetworking.get("http://www.kairos-dev.tk/images")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // do anything with response
                        Log.d("ResponseFromServer", response.toString());
                    }
                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void doUpload(File file)
    {
        try
        {
            AndroidNetworking.upload("http://www.kairos-dev.tk/images/"+file.getName())
                    .addMultipartFile("image", file)
                    .setTag("UploadImage")
                    .setPriority(Priority.IMMEDIATE)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {

                        }
                    })
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // do anything with response
                            Log.d("ServerPostResponse", response.toString());
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                            Log.d("ServerPostError", error.toString());
                        }
                    });
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        populateList();
    }

    public void uploadImage(View view)
    {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (cameraIntent.resolveActivity(getPackageManager()) != null)
        {
            try
            {
                this.photoFile = FileUtils.createFile(this, ".jpg");
                mCurrentPhotoPath = photoFile.getAbsolutePath();
//                Toast.makeText(this,mCurrentPhotoPath, Toast.LENGTH_LONG).show();
                this.photoURI = FileProvider.getUriForFile(this, "com.example.calebe.im_up_app.fileprovider", photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
//                new CallAPI().execute(mCurrentPhotoPath);
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private File getPhoto(String file_path)
    {
        String path = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString();
        File directory = new File(path);
        File[] files = directory.listFiles();
        File file_to_return = null;
        for (int i = 0; i < files.length; i++)
        {
            if(file_path.contains(files[i].getAbsolutePath()))
            {
                file_to_return = files[i];
            }
        }
        return file_to_return;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            try
            {
//                doUpload(this.photoFile);
                (new CallAPI(this)).execute(mCurrentPhotoPath);
            } catch (Exception e)
            {
                e.printStackTrace();
            }

        }else if(resultCode == RESULT_CANCELED)
        {
            Toast.makeText(this,mCurrentPhotoPath, Toast.LENGTH_LONG).show();
            if(this.photoFile != null  && mCurrentPhotoPath != null)
            {
                this.photoFile = new File(mCurrentPhotoPath);
                this.photoFile.delete();
                this.photoFile = null;
            }
        }

        if (requestCode == 0)
        {

            if (resultCode == RESULT_OK) {
                String contents = data.getStringExtra("SCAN_RESULT");
                Log.d("QR CODE", contents);
                (new DownloadTask(this)).execute(contents);

            }
            if (resultCode == RESULT_CANCELED) {
                //handle cancel
            }
        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if(this.photoFile != null)
        {
            this.photoFile = new File(mCurrentPhotoPath);
            this.photoFile.delete();
            this.photoFile = null;
        }
    }


        public void downloadImage(View view)
    {
        try {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.putExtra("SCAN_MODE", "QR_CODE_MODE"); // "PRODUCT_MODE for bar codes

            startActivityForResult(intent, 0);

        } catch (Exception e) {

            Uri marketUri = Uri.parse("market://details?id=com.google.zxing.client.android");
            Intent marketIntent = new Intent(Intent.ACTION_VIEW,marketUri);
            startActivity(marketIntent);

        }
    }
}

